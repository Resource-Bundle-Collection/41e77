# 小米AC2100|AX1800/AX3600/cr660x|红米AC2100/AX5/AX6/AX6S固件整理

## 简介
本仓库整理了小米AC2100、AX1800、AX3600、cr660x以及红米AC2100、AX5、AX6、AX6S等路由器的固件资源。这些固件包括官方固件和第三方编译的固件，每一款固件都经过亲测使用，确保稳定性和实用性。

## 固件来源
- **官方固件**: 来自小米官方发布的最新固件。
- **第三方固件**: 来自恩山论坛等社区中大神编译的固件，经过严格筛选，确保质量和稳定性。

## 使用说明
1. **下载前请仔细阅读README.txt文件**，了解固件的具体使用方法和注意事项。
2. 建议在刷机前备份原有固件，以防数据丢失。
3. 刷机有风险，操作需谨慎，建议在熟悉相关知识后再进行操作。

## 固件列表
- 小米AC2100官方固件
- 小米AX1800官方固件
- 小米AX3600官方固件
- 小米cr660x官方固件
- 红米AC2100官方固件
- 红米AX5官方固件
- 红米AX6官方固件
- 红米AX6S官方固件
- 第三方编译固件（OpenWrt、Padavan等）

## 更新日志
- 最新推荐文章于2024-08-23 19:54:36发布
- 本帖固件持续更新，请收藏网盘链接

## 版权声明
本文为博主原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接和本声明。

## 联系我们
如有任何问题或建议，请联系仓库维护者。